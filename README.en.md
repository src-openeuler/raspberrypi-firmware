# raspberrypi-firmware

#### Description
Firmware for RaspberryPi. These firmwares are from the repositories in RaspberryPi community.

- https://github.com/raspberrypi/firmware
- https://github.com/RPi-Distro/bluez-firmware.git
- https://github.com/RPi-Distro/firmware-nonfree.git

#### Software Architecture

AArch64

#### Installation

`dnf install raspberrypi-firmware`

#### Instructions

After installing raspberrypi-firmware,

1.  Files from firmware repository will be stored in the directory of `/boot`.
2.  Files from bluez-firmware and firmware-nonfree repositories will be stored in the directory of `/usr/lib/firmware`.

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request
