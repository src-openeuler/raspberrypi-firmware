%global _lib_path /usr/lib/firmware
%global _license_path /usr/share/licenses/raspi
%global firmware_release 1.20241126
%global debug_package %{nil}

Name:		raspberrypi-firmware
Version:	20241207
Release:	13
Summary:	Firmware files used for RaspberryPi
License:	GPL+ and GPLv2+ and Redistributable, no modification permitted and BCM_LEGAL and SYNAPTICS-END-USER-SOFTWARE-TOOL-LICENSE and binary-redist-Cypress and binary-redist-Broadcom-wifi and Synaptics and broadcom_bcm43xx
ExclusiveArch: aarch64

Provides:	raspberrypi-firmware = %{version}-%{release}

URL:		https://github.com/raspberrypi/firmware
Source0:	https://github.com/raspberrypi/firmware/archive/%{firmware_release}/firmware-%{firmware_release}.tar.gz
Source1:	bluez-firmware-1.2-9+rpt3.tar.gz
Source2:	firmware-nonfree-20230625-2+rpt3.tar.gz
Conflicts:	linux-firmware

%description
This package contains firmware images required by some RaspberryPi devices.

%prep
%setup -q -n %{name}-%{version} -a 1 -a 2 -c
mv bluez-firmware-* bluez-firmware
mv firmware-nonfree-* firmware-nonfree

%install
mkdir -p %{buildroot}%{_lib_path}/brcm %{buildroot}%{_lib_path}/synaptics %{buildroot}%{_lib_path}/cypress
mkdir -p %{buildroot}%{_license_path}
mkdir -p %{buildroot}/boot
cd bluez-firmware
install -p -m 644 broadcom/BCM2033-FW.bin %{buildroot}%{_lib_path}
install -p -m 644 broadcom/BCM2033-MD.hex %{buildroot}%{_lib_path}
install -p -m 644 st/* %{buildroot}%{_lib_path}/
install -p -m 644 debian/firmware/broadcom/*.hcd %{buildroot}%{_lib_path}/brcm
install -p -m 644 debian/firmware/synaptics/*.hcd %{buildroot}%{_lib_path}/synaptics
# install -p -m 644 broadcom/BCM-LEGAL.txt %{buildroot}%{_license_path}
# install -p -m 644 debian/firmware/broadcom/LICENSE.cypress %{buildroot}%{_license_path}
# install -p -m 644 debian/firmware/synaptics/LICENSE.synaptics %{buildroot}%{_license_path}
install -p -m 644 debian/copyright %{buildroot}%{_license_path}/LICENCE.bluez-firmware

cd ../firmware-nonfree
cp -a brcm/* %{buildroot}%{_lib_path}/brcm
install -p -m 644 cypress/* %{buildroot}%{_lib_path}/cypress
install -p -m 644 LICENCE.* %{buildroot}%{_license_path}

cd ../firmware-%{firmware_release}
install -p -m 644 boot/*.bin %{buildroot}/boot
install -p -m 644 boot/*.linux %{buildroot}/boot
install -p -m 644 boot/*.dat %{buildroot}/boot
install -p -m 644 boot/*.elf %{buildroot}/boot
install -p -m 644 boot/LICENCE.* %{buildroot}/boot

cd %{buildroot}%{_lib_path}/brcm
ln -sf ../cypress/cyfmac43455-sdio-minimal.bin cyfmac43455-sdio.bin

cd -

%files
%dir %{_lib_path}
%{_lib_path}/*
/boot/*
%license %{_license_path}

%changelog
* Tue Dec 17 2024 Yafen Fang<yafen@iscas.ac.cn> - 20241207-13
- add license: broadcom_bcm43xx
- preserve the symbolic link format when installing firmwares of brcm

* Mon Dec 9  2024 Yafen Fang<yafen@iscas.ac.cn> - 20241207-12
- update license

* Sat Dec 7  2024 Yafen Fang<yafen@iscas.ac.cn> - 20241207-11
- update firmware version to 1.20241126
- update bluez-firmware to the latest commit (78d6a07730e2d20c035899521ab67726dc028e1c): Update changelog for 1.2-9+rpt3 release
- update firmware-nonfree to the latest commit (4b356e134e8333d073bd3802d767a825adec3807): Update changelog for 1:20230625-2+rpt3 release

* Mon Nov 4  2024 Yafen Fang<yafen@iscas.ac.cn> - 20240419-10
- update firmware version to 1.20241008

* Fri Apr 19 2024 Yafen Fang<yafen@iscas.ac.cn> - 20240419-9
- update firmware to version 1.20240306
- update bluez-firmware to the latest commit (5a204dae9b3dc521842c56d93f6b4ecfbe4ff44d): 1.2-4+rpt11 release

* Tue Dec 19 2023 Yafen Fang<yafen@iscas.ac.cn> - 20231219-8
- delete files: regulatory.db* and LICENCE.wireless-regdb

* Wed Mar 15 2023 Yafen Fang<yafen@iscas.ac.cn> - 20230315-7
- update firmware to version 1.20230306
- update bluez-firmware to the latest commit (9556b08ace2a1735127894642cc8ea6529c04c90): 1.2-4+rpt10 release

* Wed Mar 16 2022 Yafen Fang<yafen@iscas.ac.cn> - 20220316-6
- update firmware to version 1.20220308
- update firmware-nonfree to the latest commit (54ffdd6e2ea6055d46656b78e148fe7def3ec9d8): 1:20190114-2+rpt4 release

* Tue Feb 23 2021 Yafen Fang<yafen@iscas.ac.cn> - 20210223-5
- update firmware files: firmware(1.20210201), bluez-firmware(1.2-4+rpt8: e7fd166981ab4bb9a36c2d1500205a078a35714d) and firmware-nonfree(1:20190114-1+rpt11: 83938f78ca2d5a0ffe0c223bb96d72ccc7b71ca5).

* Tue Nov 10 2020 Yafen Fang<yafen@iscas.ac.cn> - 20201110-4
- update firmware files: firmware(1.20201022), bluez-firmware(a4e08822e3f24a6211f6ac94bc98b7ef87700c70) and firmware-nonfree(5113681d6dcd46581a1882cbeb3d5cf1ddbf7676).

* Tue Sep 22 2020 Yafen Fang<yafen@iscas.ac.cn> - 20200904-3
- Add the conflicts field, declaring the conflicts with linux-firmware(brcmfmac43362-sdio.lemaker,bananapro.txt/brcmfmac43430-sdio.bin/brcmfmac43455-sdio.bin in /usr/lib/firmware/brcm).

* Wed Sep 16 2020 Yafen Fang<yafen@iscas.ac.cn> - 20200904-2
- Change source0's name.

* Fri Sep 4  2020 Yafen Fang<yafen@iscas.ac.cn> - 20200904-1
- Update firmware files.

* Mon Aug 31 2020 Yafen Fang<yafen@iscas.ac.cn> - 20200717-3
- Add source url in spec.

* Fri Jul 17 2020 Yafen Fang<yafen@iscas.ac.cn> - 20200717-1
- Update firmware files, add licenses.

* Wed May 20 2020 Jianmin Wang<jianmin@iscas.ac.cn> - 20200512-1
- Add firmware file from RaspberryPi firmware, bluez-firmware and firmware-nonfree.
