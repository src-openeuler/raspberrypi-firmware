# raspberrypi-firmware

#### 介绍

用于打包 Raspberry Pi 的固件文件。固件文件来自于 Raspberry Pi 社区的上游仓库：

- https://github.com/raspberrypi/firmware
- https://github.com/RPi-Distro/bluez-firmware.git
- https://github.com/RPi-Distro/firmware-nonfree.git

#### 软件架构

AArch64

#### 安装教程

`dnf install raspberrypi-firmware`

#### 使用说明

安装 raspberrypi-firmware 后，

1. firmware 仓库下的固件等文件覆盖到树莓派 `/boot` 目录下。
2. bluez-firmware 和 firmware-nonfree 仓库下的固件等文件覆盖到 `/usr/lib/firmware` 下。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
